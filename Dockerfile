FROM ubuntu:22.04

COPY ./ /azrs

WORKDIR /azrs

RUN apt-get update
RUN apt-get install -y doxygen graphviz

CMD ["sh", "-c", "doxygen Doxyfile"]
